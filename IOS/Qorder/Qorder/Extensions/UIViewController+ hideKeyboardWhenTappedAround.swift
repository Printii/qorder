//
//  UIViewController+ hideKeyboardWhenTappedAround.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/2/1.
//  Copyright © 2017年 Printii. All rights reserved.
//
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
