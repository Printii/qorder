//
//  Array+remove.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/2/6.
//  Copyright © 2017年 Printii. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
