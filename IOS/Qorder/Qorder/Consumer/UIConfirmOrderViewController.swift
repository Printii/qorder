//
//  UIConfirmOrderViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/19.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase
import EVReflection

class UIConfirmOrderViewController : UITableViewController {
    var business:Business?
    var order:Bill?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setToolbarHidden(true, animated: animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "consumerInfo" && business != nil && order != nil {
            let vc = segue.destination as! UIConsumerInfoViewController
            vc.providerId = self.business!.id
            
            if let order = self.order {
                let profile = self.business!.profile
                let biz = self.business!
                order.providerInfo.name = profile.name
                order.providerInfo.address = profile.address
                order.providerInfo.phone = profile.phone
                order.providerInfo.ownerId = biz.owner
                order.providerInfo.businessId = biz.id
                
                vc.order = order
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.order == nil || self.order?.items.count == 0 ? 1 : self.order?.items.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = self.order == nil || self.order?.items.count == 0 ? "empty" : "Item"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid)
        
        if cellid == "Item" {
            let item = self.order?.items[indexPath.row]
            let name = item?.name ?? ""
            let price = item?.price ?? 0.0
            let quantity = item?.quantity ?? 0
            let subtotal = item?.subtotal ?? 0.0
            
            cell?.textLabel?.text = name
            cell?.detailTextLabel?.text = "\(price) x\(Int(quantity)) = \(subtotal)"
        }
        
        return cell!
    }
}
