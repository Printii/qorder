//
//  UIProviderHomeViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/17.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit

class UIProviderHomeViewController : UITableViewController {
    var Business:Business?
    var editor:BillEditor = BillEditor()
    
    override func viewDidLoad() {
        editor.newBill()
        self.navigationController?.navigationBar.tintColor = .blue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.Business?.products.count)!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Product")
        let prd = self.Business?.products[indexPath.row]
//        let dict = self.Business?.products
//        let prd = Array(dict!.values)[indexPath.row]
        
        let name = prd?.name
        let price = prd?.price
        let quantity = editor.numberOfProduct(product: prd!)
        
        let imgPhoto = cell?.viewWithTag(99) as? UIImageView
        let lbName = cell?.viewWithTag(1) as? UILabel
        let lbPrice = cell?.viewWithTag(2) as? UILabel
        let lbQuantity = cell?.viewWithTag(3) as? UILabel
        let btnAdd = cell?.viewWithTag(10) as? UIButton
        let btnRemove = cell?.viewWithTag(11) as? UIButton
        
        lbName?.text = name
        lbPrice?.text = String (format:"$%0.0f", price!)
        lbQuantity?.text = String(format:"%i", quantity)
        imgPhoto?.downloadedFrom(link: (prd?.image)!)
        btnAdd?.addTarget(self,
            action: #selector(UIProviderHomeViewController.btnAddClicked(sender:)),
            for: .touchUpInside)
        btnRemove?.addTarget(self,
            action: #selector(UIProviderHomeViewController.btnRemoveClicked(sender:)),
            for: .touchUpInside)
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "shopcart" {
            let nextctrl = segue.destination as? UIConfirmOrderViewController
            nextctrl?.business = self.Business
            nextctrl?.order = self.editor.save()
        }
    }
    
    func btnAddClicked(sender:AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        let prd = self.Business?.products[indexPath.row]
//        let dict = self.Business?.products
//        let prd = Array(dict!.values)[indexPath.row]
        self.editor.add(product: prd!)
        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        update()
    }
    
    func btnRemoveClicked(sender:AnyObject){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        let prd = self.Business?.products[indexPath.row]
//        let dict = self.Business?.products
//        let prd = Array(dict!.values)[indexPath.row]
        self.editor.remove(product: prd!)
        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        update()
    }
    
    func update(){
        let total = editor.total()
        self.title = total == 0 ? "" :
            String (format: "合計 %0.0f 元", editor.total())
    }
}
