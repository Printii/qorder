//
//  UIConsumerInfoViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/31.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class UIConsumerInfoViewController : UIViewController {
    var providerId:String?
    var order:Bill?
    var firstResponder:UIView?
    @IBOutlet weak var segGender: UISegmentedControl!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtMemo: UITextView!
    @IBOutlet weak var dpPreferTime: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)
        
        let core = AppCore.sharedInstance()
        
        let user = core.currentUser
        let profile = core.currentUserProfile
        txtName.text = user?.displayName ?? ""
        txtPhone.text = profile?.phone ?? ""
        
        let time = Date(timeIntervalSinceNow: 1800)
        self.dpPreferTime.minimumDate = time
        // self.dpPreferTime.maximumDate = time
        self.dpPreferTime.date = time
        self.order?.consumerInfo.preferDeliverTime = time.timeIntervalSince1970
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setToolbarHidden(true, animated: animated)
    }
    
    @IBAction func onPreferTimeChanged(_ sender: Any) {
        if self.order == nil { return }
        
        self.order?.consumerInfo.preferDeliverTime = self.dpPreferTime.date.timeIntervalSince1970
    }
    
    
    @IBAction func checkout(_ sender: Any) {
        if self.order == nil || self.providerId == nil { return }
        
        var gender = ""
        switch segGender.selectedSegmentIndex {
            case 1:
            gender = "先生"
            case 2:
            gender = "小姐"
            default:
            gender = ""
        }
        
        let n = txtName?.text ?? ""
        let name = "\(n)\(gender)"
        let phone = txtPhone.text!
        let memo = txtMemo.text!
        
        self.order?.consumerInfo.name = name
        self.order?.consumerInfo.phone = phone
        self.order?.consumerInfo.address = ""
        self.order?.consumerInfo.memo = memo
        
        AppCore.sharedInstance()
            .request(
                order: self.order!,
                consumerPhone: phone,
                providerId: providerId!,
                completion: { error in
                    if let err = error {
                        print("send order fail")
                        print(err)
                        Util.alert(
                            vc: self, title: "無法送出訂單",
                            message: "請稍候再重試一次。（\(err.localizedDescription)）")
                        return
                    }
                    
                    print("order send success")
                    Util.alert(
                        vc: self, title: "訂單已送出",
                        message: "謝謝惠顧，請於指定時間至店內結帳取餐。若需查詢訂單內容，請至「我的訂單」查詢",
                        completion: { (vc) in
                            self.navigationController?.popToRootViewController(animated: true)
                    })
            })
    }
}
