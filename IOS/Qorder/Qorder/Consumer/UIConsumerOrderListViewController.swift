//
//  UIConsumerOrderListViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/2/1.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit

class UIConsumerOrderListViewController: UITableViewController {
    var orders:[Bill]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let core = AppCore.sharedInstance()
        
        if !core.isSignedIn { return }
        
        core.orders(consumerId: (core.currentUser?.uid)!) { (orders) in
            self.orders = orders
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if orders == nil || orders?.count == 0 { return 1 }
        return orders?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders == nil || orders?.count == 0 { return 1 }
        let count = orders?[section].items.count ?? 0
        return count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellId = indexPath.row == 0 ? "summary" : "item"
        if orders == nil || orders?.count == 0 { cellId = "empty" }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        
        if cellId == "summary" {
            let bill = (orders?[indexPath.section])!
            cell?.textLabel?.text = "交易金額 $\(bill.total)"
            cell?.detailTextLabel?.text = "\(Date(timeIntervalSince1970: bill.consumerInfo.preferDeliverTime))"
        }
        
        if cellId == "item" {
            let bill = (orders?[indexPath.section])!
            let item = bill.items[indexPath.row-1]
            cell?.textLabel?.text = item.name
            cell?.detailTextLabel?.text = "\(item.price) x\(item.quantity) = \(item.subtotal)"
        }
        
        return cell!
    }
}
