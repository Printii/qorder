//
//  UIProviderListViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/8.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase

class UIProviderListViewController: UITableViewController {
    
    var items = [Business]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppCore.sharedInstance().businesses { (businesses) in
            self.items = businesses
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(true, animated: animated)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "Business")
        let biz = self.items[indexPath.row]
        let name = biz.profile.name
        let address = biz.profile.address
        let banner = biz.profile.banner
        
        let imgView = cell?.viewWithTag(99) as! UIImageView
        let lbName = cell?.viewWithTag(1) as! UILabel
        let lbContactInfo = cell?.viewWithTag(2) as! UILabel
        
        imgView.image = nil
        imgView.downloadedFrom(link: banner)
        lbName.text = name
        lbContactInfo.text = address
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextctrl = segue.destination as? UIProviderHomeViewController
        let indexPath = tableView.indexPathForSelectedRow
        nextctrl?.Business = self.items[indexPath!.row]
    }
    
    
}
