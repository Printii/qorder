//
//  UIMyBusinessListViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/31.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class UIMyBusinessListViewController: UITableViewController {
    var businesses = [Business]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 1. check if user is login & is owner of business
        // 2. if not login, ask for login
        // 3. if login, load businesses
        
        let core = AppCore.sharedInstance()
        if core.isBusinessOwner {
            core.myBusinesses { (businesses) in
                self.businesses = businesses
                self.tableView.reloadData()
            }
        } else {
            performSegue(withIdentifier: "auth", sender: self)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.businesses.count : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = indexPath.section == 0 ? "business" : "btnNew"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)!
        
        if cellId == "business" {
            let biz = self.businesses[indexPath.row]
            cell.textLabel?.text = biz.profile.name
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOrders" {
            let nextctrl = segue.destination as? UIOrderListViewController
            let indexPath = tableView.indexPathForSelectedRow
            nextctrl?.business = self.businesses[indexPath!.row]
        }
        
        if segue.identifier == "btnNew" {
            let nextctrl = segue.destination as? UIEditBusinessViewController
            nextctrl?.business = Business()
        }
    }
}
