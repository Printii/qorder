//
//  File.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/25.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController : FormFillViewController {
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBAction func createAccount(_ sender: AnyObject) {
        let phone = txtAccount.text
        let pwd = txtPassword.text
        let cfm = txtConfirmPassword.text

        
        if phone == nil || pwd == nil || pwd != cfm {
            Util.alert(vc: self, title: "無法建立帳號", message: "請確認帳號及密碼格式後再重試一次.")
            return
        }
        
        Util.showLoading(vc: self, message: "正在建立帳號...")
        AppCore.sharedInstance().signUpAsBusinessOwner(
            phone: phone!, password: pwd! ) { (user, error) in
                Util.hideLoading()
                if let err = error {
                    Util.alert(vc: self, title: "無法建立帳號", message: err.localizedDescription)
                    return
                }
                self.verify()
        }
    }
    
    func verify() {
        // todo: 簡訊驗證
        
        Util.alert(vc: self,
            title: "帳號已建立",
            message: "您現在可以開始建立您的早餐店資料了",
            completion: { (vc) in
                self.dismiss()
        })
    }
    
    @IBAction func dismiss(_ sender: AnyObject) {
        // from cancel
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismiss() {
        // from end of flow
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

