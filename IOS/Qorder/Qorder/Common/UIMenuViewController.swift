//
//  UIMenuViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/31.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class UIMenuViewController: UITableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 4 {
            
            do{
                try AppCore.sharedInstance().signOut()
                self.navigationController?.popToRootViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            } catch {
                Util.alert(vc: self, title: "Can't logout.", message: error.localizedDescription)
            }
        }
    }
}
