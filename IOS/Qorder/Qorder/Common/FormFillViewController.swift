//
//  FormFillViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/30.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit

class FormFillViewController: UIViewController {
    var isKeyboardShown = false
    
    @IBOutlet weak var container: UIScrollView!
    
    override func viewDidLoad() {
//        let center: NotificationCenter = NotificationCenter.default
//        
//        center.addObserver(
//            self, selector: #selector(AuthViewController.keyboardWillShow(notification:)),
//            name: .UIKeyboardWillShow, object: nil)
//        center.addObserver(
//            self, selector: #selector(AuthViewController.keyboardWillHide(notification:)),
//            name: .UIKeyboardWillHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        container.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height + 44)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if isKeyboardShown { return }
        isKeyboardShown = true
        
        let info:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight: CGFloat = keyboardSize.height
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        let frame = self.container.frame
        
        UIView.animate(
            withDuration: 0.25,
            delay: 0.25,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: {
                self.container.frame = CGRect(
                    x:0,
                    y:0,
                    width: frame.size.width,
                    height: frame.size.height - keyboardHeight)
        }, completion: nil)
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if !isKeyboardShown { return }
        isKeyboardShown = false
        
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight: CGFloat = keyboardSize.height
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        let frame = self.container.frame
        
        UIView.animate(
            withDuration: 0.25,
            delay: 0.25,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: {
                self.container.frame = CGRect(
                    x:0,
                    y:0,
                    width: frame.size.width,
                    height: frame.size.height + keyboardHeight)
        }, completion: nil)
        
    }
}
