//
//  ResetPassordViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/25.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class ResetPasswordViewController : FormFillViewController {
    @IBOutlet weak var txtEMail: UITextField!
    
    @IBAction func resetPassword(_ sender: AnyObject) {
        let email = txtEMail.text
        
        if email == nil { return }
        
        FIRAuth.auth()?.sendPasswordReset(
            withEmail: email!,
            completion: { (error) in
                if let err = error {
                    Util.alert(vc: self, title: "Reset Password Failed.", message: err.localizedDescription)
                    return
                }
                
                self.dismiss()
        })
    }
    
    @IBAction func dismiss(_ sender: AnyObject) {
        // from cancel
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismiss() {
        // from end of flow
        self.navigationController?.popToRootViewController(animated: true)
    }
}
