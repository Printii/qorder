//
//  AuthViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/25.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import GoogleSignIn

class AuthViewController : FormFillViewController, FBSDKLoginButtonDelegate {
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var FBBtnContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let width = self.FBBtnContainer.frame.size.width
        let height = self.FBBtnContainer.frame.size.height
        let btn = FBSDKLoginButton()
        btn.frame = CGRect(x:0, y:0, width: width, height: height)
        btn.center = CGPoint(x: width/2, y: height/2)
        btn.delegate = self
        self.FBBtnContainer.addSubview(btn)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let core = AppCore.sharedInstance()
        if core.isSignedIn {
            txtAccount.text = core.currentUserProfile?.phone ?? ""
        }
    }
    
    @IBAction func proceedLoginFlow(_ sender: AnyObject) {
        let acc = txtAccount.text
        let pwd = txtPassword.text
        
        if acc == nil || pwd == nil { return }
        
        Util.showLoading(vc: self, message: "登入中...")
        AppCore.sharedInstance().signIn(
            phone: acc!, password: pwd!,
            completion: { (user, error) in
                Util.hideLoading()
                
                if let err = error {
                    Util.alert(vc: self, title: "Login Failed.", message: err.localizedDescription)
                    return
                }
                
                self.dismiss()
        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
        Util.alert(vc: self, title: "Logout", message: "Logout successful.")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if let err = error{
            print(err.localizedDescription)
            return
        }
        
        Util.showLoading(vc: self, message: "登入中...")
        AppCore.sharedInstance().signIn(
            token: FBSDKAccessToken.current().tokenString) { (user, error) in
                Util.hideLoading()
                if let err = error {
                    print(err.localizedDescription)
                    // todo: show error message
                    return
                }
                
                self.dismiss()
        }
    }
    
    func dismiss() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

