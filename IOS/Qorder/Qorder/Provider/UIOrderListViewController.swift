//
//  UIOrderListViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/28.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit

class UIOrderListViewController: UITableViewController {
    var CELL_BILLSUM = "summary"
    var CELL_BILLITEM = "item"
    var business:Business?
    var orders = [Bill]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnEdit = UIBarButtonItem(
            barButtonSystemItem: UIBarButtonSystemItem.edit,
            target: self,
            action: #selector(UIOrderListViewController.onEdit(_:)))
        self.navigationItem.rightBarButtonItem = btnEdit
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.orders.removeAll()
        self.tableView.reloadData()
        self.reload()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.orders.removeAll()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.orders.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let order = self.orders[section]
        return 1 + order.items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 100.0 : 44.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = indexPath.row == 0 ? CELL_BILLSUM : CELL_BILLITEM
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)!
        let b = self.orders[indexPath.section]
        
        if cellId == CELL_BILLSUM {
            //cell.textLabel?.text = "\(b.total) 元"
            let txtCustomerName = cell.viewWithTag(1) as? UILabel
            let txtPhone = cell.viewWithTag(2) as? UILabel
            let txtMemo = cell.viewWithTag(3) as? UILabel
            let sgmState = cell.viewWithTag(4) as? UISegmentedControl
            let btnCheckout = cell.viewWithTag(5) as? UIButton
            
            var preferTime = b.consumerInfo.preferDeliverTime
            if preferTime == nil {
                preferTime = b.createTime + 1800
            }
            let pdate = Date(timeIntervalSince1970: preferTime)
            let outFormatter = DateFormatter()
            //outFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            outFormatter.dateFormat = "hh:mm"
            txtCustomerName?.text = "\(outFormatter.string(from: pdate)) \(b.consumerInfo.name) \(b.total) 元"
            txtPhone?.text = b.consumerInfo.phone
            
            outFormatter.dateFormat = "yyyy/MM/dd"
            txtMemo?.text = "\(outFormatter.string(from:pdate))\n\(b.consumerInfo.memo)"
            switch b.state {
            case Bill.STATE_WAIT_FOR_PAYMENT:
                sgmState?.selectedSegmentIndex = 1
            default:
                sgmState?.selectedSegmentIndex = 0
            }
            
            sgmState?.addTarget(
                self,
                action: #selector(UIOrderListViewController.onStateChanged(sender:)),
                for: UIControlEvents.valueChanged)
            
            btnCheckout?.addTarget(
                self,
                action: #selector(UIOrderListViewController.onCheckout(sender:)),
                for: UIControlEvents.touchUpInside)
            
        } else {
            let item = b.items[indexPath.row-1]
            cell.textLabel?.text = item.name
            cell.detailTextLabel?.text = "x \(item.quantity) = $ \(item.subtotal)"
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit" {
            let vc = segue.destination as? UIEditBusinessViewController
            vc?.business = self.business
        }
    }
    
    func onEdit(_ sender:Any) {
        self.performSegue(withIdentifier: "edit", sender: self)
    }
    
    func onStateChanged(sender:AnyObject) {
        let sgm = sender as? UISegmentedControl
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        if indexPath.row > 0 { return }
        let order = self.orders[indexPath.section]
        order.state = sgm?.selectedSegmentIndex == 0 ?
            Bill.STATE_PREPARING : Bill.STATE_WAIT_FOR_PAYMENT
    }
    
    func onCheckout(sender:AnyObject) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)!
        
        if indexPath.row > 0 { return }
        let order = self.orders[indexPath.section]
        
        AppCore.sharedInstance()
            .checkoutWithCash(order: order, cash: order.total)
        
        let name = order.consumerInfo.name
        let total = order.total
        
        let title = "\(name)結帳"
        let message = "總計 $\(total) 元"
        
        Util.alert(vc: self, title: title, message: message)
    }
    
    func reload() {
        if self.business == nil { return }
        AppCore.sharedInstance().orders(providerId: self.business!.id) { (orders) in
            self.orders = orders
            self.tableView.reloadData()
        }
    }
}
