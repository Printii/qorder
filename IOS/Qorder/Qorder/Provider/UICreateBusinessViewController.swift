//
//  UICreateBusinessViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/27.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class UICreateBusinessViewController : UITableViewController {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var imgBanner: UIImageView!
    
    func updateBanner(business:Business, image:UIImage) {
        let storageRef = FIRStorage.storage().reference()
        let bannerRef = storageRef.child("/\(AppCore.FIR_DB_BIZ)/\(business.id)/banner.png")
        let data = UIImagePNGRepresentation(image)
        
        if (data == nil) { return }
        
        bannerRef.put(data!, metadata: nil) { (metadata, error) in
            if let err = error {
                Util.alert(vc: self, title: "Can't upload banner.", message: err.localizedDescription)
            }
            
            let ref = FIRDatabase.database().reference()
            let url = metadata!.downloadURL()
            business.profile.banner = "\(url)"
            ref.child("\(AppCore.FIR_DB_BIZ)/\(business.id)").setValue(business.toDictionary())
        }
    }
    
    func updateProfile(business:Business) {
        let name = txtName.text ?? ""
        let address = txtAddress.text ?? ""
        let ref = FIRDatabase.database().reference()
        let profileRef = ref.child("\(AppCore.FIR_DB_BIZ)/\(business.id)/profile")
        
        let profile = Profile()
        profile.name = name
        profile.address = address
        profile.banner = ""
        
        profileRef.setValue(profile.toDictionary())
        
        let title = "Update Profile"
        let msg = "Profile updated."
        
        Util.alert(vc: self, title: title, message: msg) { (vc) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func create() {
        let name = txtName.text ?? ""
        let address = txtAddress.text ?? ""
        let owner = FIRAuth.auth()?.currentUser
        let ref = FIRDatabase.database().reference()
        let bizRef = ref.child(AppCore.FIR_DB_BIZ).childByAutoId()
        
        if owner == nil {
            Util.alert(vc: self, title: "Can't create new Business.", message: "Please create an account before registering a new business.")
            return
        }
        
        let business = Business()
        business.id = bizRef.key
        business.owner = owner!.uid
        business.profile = Profile()
        business.profile.name = name
        business.profile.address = address
        business.profile.banner = "" // todo: upload a banner
        
        bizRef.setValue(business.toDictionary())
        
        let title = "Create Business"
        let msg = "Business Created, now you can edit your business at 'My Business'."
        
        Util.alert(vc: self, title: title, message: msg) { (vc) in
            self.dismiss(animated: true, completion: nil)
        }
    }
}
