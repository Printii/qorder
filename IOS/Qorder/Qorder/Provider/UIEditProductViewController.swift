//
//  Info.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/27.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit
import Firebase

class UIEditProductViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var business:Business?
    var product:Product?
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.updateView()
    }
    
    @IBAction func sync(_ sender: Any) {
        if self.business == nil { return }
        if self.product == nil { return }
        
        let name = txtName.text ?? ""
        let price = txtPrice.text ?? "0"
        
        self.product?.name = name
        self.product?.price = Double(price) ?? 0
        self.save()
    }
    
    @IBAction func changePhoto(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info [UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgPhoto.image = pickedImage
            
            if self.product == nil || self.business == nil { return }
            
            AppCore.sharedInstance().upload(
                productImage: pickedImage.resized(toWidth: 640)!,
                product: self.product!,
                business: self.business!) { (path, error) in
                    if let err = error {
                        Util.alert(vc: self, title: "照片上傳失敗", message:
                        err.localizedDescription)
                        return
                    }
                    let p = path
                    self.product?.image = p
                    self.save()
            }
        }
    }
    
    func updateView() {
        let path = (self.product?.image)!
        let name = self.product?.name ?? ""
        let price = self.product?.price ?? 0
        
        imgPhoto.downloadedFrom(link: path)
        txtName.text = name
        txtPrice.text = "\(price)"
    }
    
    func save() {
        if business == nil { return }
        AppCore.sharedInstance().save(business: business!)
    }
    
    @IBAction func onSave() {
        self.save()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onDelete() {
        if self.product == nil { return }
        if self.business == nil { return }
        
        let name = self.product?.name ?? ""
        
        self.business?.products.remove(object: self.product!)
        self.save()
        Util.alert(vc: self, title: "\(name)已刪除", message: "") { (vc) in
            vc.navigationController?.popViewController(animated: true)
        }
    }
    
}
