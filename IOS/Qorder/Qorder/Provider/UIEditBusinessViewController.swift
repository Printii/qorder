//
//  UIEditBusinessViewController.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/2/2.
//  Copyright © 2017年 Printii. All rights reserved.
//

import UIKit

class UIEditBusinessViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var business:Business?
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnDone = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(UIEditBusinessViewController.onDone(_:)))
        self.navigationItem.rightBarButtonItem = btnDone
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.updateView()
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "商品列表"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.business?.products.count ?? 0) + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let prdcnt = business?.products.count ?? 0
        let cellId = indexPath.row >= prdcnt ? "btnNew" : "product"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId)!
        
        if cellId == "product" {
            let prd = self.business?.products[indexPath.row]
            let price = prd!.price
            cell.textLabel?.text = prd?.name
            cell.detailTextLabel?.text = "$\(price)"
            cell.imageView?.downloadedFrom(link: (prd?.image)!)
            cell.imageView?.frame = CGRect(x: 0, y: 0, width: 44, height:44)
            cell.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "product" {
            let vc = segue.destination as? UIEditProductViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            let prd = self.business?.products[indexPath.row]
            vc?.product = prd
            vc?.business = self.business
        }
        
        if segue.identifier == "btnNew" {
            let vc = segue.destination as? UIEditProductViewController
            let uuid = UUID.init().uuidString
            let product = Product()
            
            product.id = uuid
            
            if let business = self.business {
                self.business?.products.append(product)
            }
            
            vc?.product = product
            vc?.business = self.business
        }
    }
    
    @IBAction func sync(_ sender: Any) {
        let name = txtName.text ?? ""
        let address = txtAddress.text ?? ""
        let phone = txtPhone.text ?? ""
        
        business?.profile.name = name
        business?.profile.address = address
        business?.profile.phone = phone
        
        self.save()
    }
    
    @IBAction func changeBanner(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info [UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgBanner.image = pickedImage
            
            AppCore.sharedInstance().upload(
                bannerImage: pickedImage.resized(toWidth: 640)!,
                business: self.business!,
                completion: { (path, error) in
                    if let err = error {
                        Util.alert(vc: self, title: "無法上傳照片", message: err.localizedDescription)
                    }
                    
                    self.business?.profile.banner = path
                    AppCore.sharedInstance().save(business: self.business!)
            })
        }
    }
    
    func updateView() {
        if let profile = business?.profile {
            imgBanner.downloadedFrom(link: profile.banner)
            txtName.text = profile.name
            txtAddress.text = profile.address
            txtPhone.text = profile.phone
        }
    }
    
    func save(){
        if business == nil { return }
        AppCore.sharedInstance().save(business: business!)
    }
    
    func onDone(_ sender:Any) {
        save()
        self.navigationController?.popViewController(animated: true)
    }
}
