//
//  Business.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/17.
//  Copyright © 2017年 Printii. All rights reserved.
//

import Foundation
import EVReflection

class Profile:EVObject {
    var name = ""
    var address = ""
    var phone = ""
    var banner = ""
}

class Option:EVObject {
    var name = ""
    var price = 0.0
}

class Product:EVObject {
    var id = ""
    var image = ""
    var name = ""
    var price = 0.0
}

class Business:EVObject {
    var id = ""
    var owner = ""
    var profile = Profile()
    var products = [Product]()
}
