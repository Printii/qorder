//
//  User.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/19.
//  Copyright © 2017年 Printii. All rights reserved.
//

import Foundation
import EVReflection

class UserProfile:EVObject {
    var name = ""
    var phone = ""
    var isOwner = false
    var device = ""
}
