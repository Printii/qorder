//
//  AppCore.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/18.
//  Copyright © 2017年 Printii. All rights reserved.
//

import Foundation
import Firebase
import FBSDKLoginKit
import EVReflection

class AppCore {
    
    static let FIR_STORAGE_URL = "gs://quickorder-ff7aa.appspot.com"
    static let FIR_DB_BIZ = "businesses"
    static let FIR_DB_ORDER = "orders"
    static let FIR_DB_BILL = "bills"
    
    private static var mInstance:AppCore?
    var currentUserProfile:UserProfile?
    var currentToken:String? // represent the apns token for current device
    
    static func sharedInstance() -> AppCore {
        if mInstance == nil {
            mInstance = AppCore()
        }
        return mInstance!
    }
    
    init() {
        if let user = FIRAuth.auth()?.currentUser {
            retriveUserProfile(user: user)
        }
    }
    
    // properties
    
    var isSignedIn:Bool { return FIRAuth.auth()?.currentUser != nil }
    var isBusinessOwner:Bool {
        return self.currentUserProfile?.isOwner ?? false
    }
    var currentUser:FIRUser? { return FIRAuth.auth()?.currentUser }
    
    // services
    
    func signUp(
        phone:String, password:String,
        completion: @escaping (_ user:FIRUser?, _ error:Error?)->Void) {
        let account = "\(phone)@printii.com"
        let profile = UserProfile()
        profile.name = "店家"
        profile.isOwner = false
        
        FIRAuth.auth()?.createUser(
            withEmail: account, password: password,
            completion: { (user, error) in
                if let user = user {
                    self.updateUserProfile(
                        user: user, profile: profile)
                    self.currentUserProfile = profile
                    self.updateToken(user: user)
                }
                completion(user, error)
        })
    }
    
    func signUpAsBusinessOwner(
        phone:String, password:String,
        completion: @escaping (_ user:FIRUser?, _ error:Error?)->Void) {
        
        let account = "\(phone)@printii.com"
        let profile = UserProfile()
        profile.name = "店家"
        profile.isOwner = true
        
        // todo:
        
        FIRAuth.auth()?.createUser(
            withEmail: account, password: password,
            completion: { (user, error) in
                if let user = user {
                    self.updateUserProfile(
                        user: user, profile: profile)
                    self.currentUserProfile = profile
                    self.updateToken(user: user)
                }
                completion(user, error)
        })
    }
    
    func signIn(phone:String, password:String, completion: @escaping (_ user:FIRUser?, _ error:Error?)->Void) {
        let account = "\(phone)@printii.com"
        FIRAuth.auth()?.signIn(
            withEmail: account, password: password,
            completion: { (user, error) in
                if let user = user {
                    self.retriveUserProfile(user: user, completion: {
                        completion(user, error)
                    })
                    self.updateToken(user: user)
                    return
                }
                completion(user, error)
        })
    }
    
    func signIn(token:String, completion: @escaping (_ user:FIRUser?, _ error:Error?)->Void) {
        // sign in with facebook
        let cred = FIRFacebookAuthProvider.credential(withAccessToken: token)
        FIRAuth.auth()?.signIn(with: cred, completion: { (user, error) in
            if let user = user {
                self.retriveUserProfile(user: user, completion: {
                    completion(user, error)
                })
                self.updateToken(user: user)
                return
            }
            completion(user, error)
        })
    }
    
    func signOut() throws {
        try FIRAuth.auth()?.signOut()
        self.currentUserProfile = nil
        FBSDKLoginManager.init().logOut()
    }
    
    private func updateUserProfile(user:FIRUser, profile:UserProfile) {
        let db = FIRDatabase.database().reference()
        db.child("users/\(user.uid)").setValue(profile.toDictionary())
    }
    
    private func retriveUserProfile(user:FIRUser){
        self.retriveUserProfile(user: user, completion: {
            // nothing to do
        })
    }
    private func retriveUserProfile(user:FIRUser, completion: @escaping ()->Void) {
        // makesure user exist
        let db = FIRDatabase.database().reference()
        db.child("users/\(user.uid)")
            .observeSingleEvent(of: .value, with: { (snapshot) in
                let profile = snapshot.value as? NSDictionary
                self.currentUserProfile = profile == nil ?
                    UserProfile() :
                    UserProfile(dictionary: profile!)
                completion()
            })
    }
    
    private func updateToken(user:FIRUser){
        if let token = FIRInstanceID.instanceID().token() {
            let db = FIRDatabase.database().reference()
            db.child("users/\(user.uid)/device").setValue(token)
        }
    }
    
    func disconnectFromFcm(){
        FIRMessaging.messaging().disconnect()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    // consumer service
    
    func orders(consumerId:String, completion: @escaping (_ result:[Bill])->Void) {
        let ref = FIRDatabase.database().reference()
        ref.child(AppCore.FIR_DB_ORDER)
            .queryOrdered(byChild: "consumerId")
            .queryEqual(toValue: consumerId)
            .observe(.value, with: { snapshot in
                var output = [Bill]()
                
                for c in snapshot.children {
                    let s = c as! FIRDataSnapshot
                    let o = s.value as! NSDictionary
                    let order = Bill(dictionary: o)
                    output.append(order)
                }
                
                output.sort(by: { b1, b2 in
                    return b2.consumerInfo.preferDeliverTime >
                        b1.consumerInfo.preferDeliverTime
                })
                
                completion(output)
            })
    }
    
    func businesses(completion: @escaping (_ result:[Business])->Void) {
        let ref = FIRDatabase.database().reference()
        ref.child(AppCore.FIR_DB_BIZ).observeSingleEvent(of: .value, with: { snapshot in
            var output = [Business]()
            for child in snapshot.children {
                let ch = child as? FIRDataSnapshot
                let biz = ch?.value as? NSDictionary
                
                if biz == nil { continue }
                
                let business = Business(dictionary: biz!)
                output.append(business)
            }
            
            completion(output)
        })
    }
    
    func businesses(ownerId:String, completion: @escaping (_ result:[Business])->Void) {
        let ref = FIRDatabase.database().reference()
        ref.child("businesses")
            .queryOrdered(byChild: "owner")
            .queryEqual(toValue: ownerId)
            .observe(.value, with: { snapshot in
                var output = [Business]()
                for child in snapshot.children {
                    let s = child as! FIRDataSnapshot
                    let o = s.value as! NSDictionary
                    let b = Business(dictionary: o)
                    output.append(b)
                }
                completion(output)
            })
    }
    
    func request(order:Bill, consumerPhone:String,
                 providerId:String,
                 completion: @escaping (_ error:Error?) -> Void) {
        let email = "\(consumerPhone)@printii.com"
        let password = consumerPhone
        let auth = FIRAuth.auth()
        
        print("login with phone \(consumerPhone)")
        
        if self.isSignedIn {
            let cuser = self.currentUser!
            
            self.request(
                order: order,
                consumerId: cuser.uid,
                providerId: providerId,
                completion: completion)
            
            return
        }
        
        auth?.signIn(withEmail: email, password: password, completion: { (user, error) in
            if let err = error {
                print(err)
                print("login faile, regist phone \(consumerPhone)")
                auth?.createUser(
                    withEmail: email,
                    password: password,
                    completion: { (ruser, rerror) in
                        if let rerr = rerror {
                            print(rerr)
                            completion(rerr)
                            return
                        }
                        
                        self.request(
                            order: order,
                            consumerId: ruser!.uid,
                            providerId: providerId,
                            completion: completion)
                })
                return
            }
            
            print("login phone success, sending order")
            self.request(
                order: order,
                consumerId: user!.uid,
                providerId: providerId,
                completion: completion)
        })
    }
    
    func request(order:Bill, consumerId:String,
                 providerId:String,
                 completion: @escaping (_ error:Error?) -> Void) {
        
        print("send order")
        
        let ref = FIRDatabase.database().reference()
        let orderRef = ref.child(AppCore.FIR_DB_ORDER).childByAutoId()
        let date = Date()
        let time = date.timeIntervalSince1970
        
        order.id = orderRef.key
        order.providerId = providerId
        order.consumerId = consumerId
        order.updateTime = time;     order.logs.append("\(date) 客戶進行行動預約點餐")
        order.createTime = time;     order.logs.append("\(date) 訂單成立")
        order.requestTime = time;    order.logs.append("\(date) 訂單已送出")
        
        orderRef.setValue(order.toDictionary())
        
        
        print("order sent")
        
        completion(nil)
        
        let consumerInfo = order.consumerInfo
        let providerInfo = order.providerInfo
        
        let message: [String:String] = ["title" : "\(consumerInfo.name)向您預定了商品"]
        let to = providerInfo.ownerId //"558308276414@gcm.googleapis.com"
        let messageId = UUID.init().uuidString
        let ttl:Int64 = 10
        
        FIRMessaging.messaging().sendMessage(
            message,
            to: to,
            withMessageID: messageId,
            timeToLive: ttl)
    }
    
    // provider services
    
    func myBusinesses(completion: @escaping (_ result:[Business])->Void) {
        let userId = FIRAuth.auth()?.currentUser?.uid ?? ""
        self.businesses(ownerId: userId, completion: completion)
    }
    
    func save(business:Business) {
        let db = FIRDatabase.database().reference()
        if business.id == "" {
            let bizRef = db.child(AppCore.FIR_DB_BIZ).childByAutoId()
            business.id = bizRef.key
            business.owner = self.currentUser?.uid ?? ""
        }
        db.child("\(AppCore.FIR_DB_BIZ)/\(business.id)").setValue(business.toDictionary())
    }
    
    func orders(providerId:String, completion: @escaping (_ result:[Bill])->Void) {
        let ref = FIRDatabase.database().reference()
        ref.child(AppCore.FIR_DB_ORDER)
            .queryOrdered(byChild: "providerId")
            .queryEqual(toValue: providerId)
            .observe(.value, with: { snapshot in
                var output = [Bill]()
                
                for c in snapshot.children {
                    let s = c as! FIRDataSnapshot
                    let o = s.value as! NSDictionary
                    let order = Bill(dictionary: o)
                    output.append(order)
                }
                output.sort(by: { b1, b2 in
                    return b2.consumerInfo.preferDeliverTime >
                        b1.consumerInfo.preferDeliverTime
                })
                completion(output)
            })
    }
    
    func orderReadyForPickup(order:Bill) {
        let ref = FIRDatabase.database().reference()
        ref.child("\(AppCore.FIR_DB_BILL)/\(order.id)/state").setValue(order.toDictionary())
    }
    
    func checkoutWithCash(order:Bill, cash:Double) {
        // todo: use transaction to protect the flow
        let payment = Payment()
        payment.method = Payment.PAY_CASH
        payment.amount = cash
        payment.changes = cash - order.total
        
        let date = Date()
        let time = date.timeIntervalSince1970
        
        order.updateTime = time;     order.logs.append("\(date) 進行現場結帳")
        order.deliverTime = time;    order.logs.append("\(date) 商品已送交客戶")
        order.checkoutTime = time;   order.logs.append("\(date) 客戶支付現金 \(cash) 元")
        order.completeTime = time;   order.logs.append("\(date) 本單已完結")
        
        // order.state = Bill.STATE_PAIED // notice: pickup orders complete as soon as paied
        order.state = Bill.STATE_COMPLETED
        
        let ref = FIRDatabase.database().reference()
        ref.child(AppCore.FIR_DB_BILL).child(order.id).setValue(order.toDictionary())
        ref.child(AppCore.FIR_DB_ORDER).child(order.id).removeValue()
    }
    
    func upload(
        productImage:UIImage, product:Product, business:Business,
        completion: @escaping (_ path:String, _ error:Error?) -> Void) {
        
        let path = "businesses/\(business.id)/product-images/\(product.id).jpg"
        let storageRef = FIRStorage.storage().reference()
        let imagesRef = storageRef.child(path)
        let data = UIImageJPEGRepresentation(productImage, 0.7)!
            //UIImagePNGRepresentation(productImage)!
        imagesRef.put(data, metadata: nil) { (metadata, error) in
            var imagepath = ""
            if let md = metadata {
                imagepath = "\(md.downloadURL()!)"
            }
            completion(imagepath, error)
        }
    }
    
    func upload(
        bannerImage:UIImage, business:Business,
        completion: @escaping (_ path:String, _ error:Error?) -> Void) {
        
        let path = "businesses/\(business.id)/banner.jpg"
        let storageRef = FIRStorage.storage().reference()
        let imagesRef = storageRef.child(path)
        let data = UIImageJPEGRepresentation(bannerImage, 0.7)!
            //UIImagePNGRepresentation(bannerImage)!
        imagesRef.put(data, metadata: nil) { (metadata, error) in
            var imagepath = ""
            if let md = metadata {
                imagepath = "\(md.downloadURL()!)"
            }
            completion(imagepath, error)
        }
    }
}
