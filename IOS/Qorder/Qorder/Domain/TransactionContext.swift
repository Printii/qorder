//
//  TransactionContext.swift
//  Qorder
//
//  Created by Johnson Wang on 2017/1/19.
//  Copyright © 2017年 Printii. All rights reserved.
//

import Foundation
import EVReflection


enum BillOperationError : Error {
    case NotSupport
    case Unknown
}

class BillEditor {
    private var bill:Bill?
    private var transactions:[Transaction] = [Transaction]()
    private var productCounter:[Product:Int] = [Product:Int]()
    
    func newBill() {
        self.bill = Bill()
    }
    
    func load(bill:Bill) {
        self.bill = bill
        self.transactions = [Transaction]()
        // todo: generate transactions accourding to given bill.items
    }
    
    func save() -> Bill {
        self.syncItems()
        self.syncAllAmounts()
        return (self.bill ?? nil)!
    }
    
    func numberOfItems() -> Int {
        return (self.bill?.items.count ?? 0)!
    }
    
    func numberOfProduct(product:Product) -> Int {
        return self.productCounter[product] ?? 0
    }
    
    func itemAtIndex(index:Int) -> BillItem {
        return (self.bill?.items[index] ?? nil)!
    }
    
    func add(product: Product) {
        self.transactions.append(Transaction(product: product))
        self.syncItems()
        self.syncAllAmounts()
    }
    
    func total() -> Double {
        return bill?.total ?? 0.0
    }
    
    func remove(product: Product) {
        let index = self.transactions.index(
            where: { $0.product == product })
        
        if index == nil { return }
        
        self.transactions.remove(at: index!)
        self.syncItems()
        self.syncAllAmounts()
    }
    
    func delete(tranIndex:Int) {
        self.transactions.remove(at: tranIndex)
        self.syncItems()
        self.syncAllAmounts()
    }
    
    private func syncItems(){
        if self.bill == nil { return }
        
        let bill = self.bill!
        
        var mapping = [Product:BillItem]()
        var prdCounter = [Product:Int]()
        for tran in self.transactions {
            if !mapping.keys.contains(tran.product!) {
                mapping[tran.product!] = BillItem()
            }
            let bitem = mapping[tran.product!]
            bitem?.name = (tran.product?.name)!
            bitem?.price = (tran.product?.price)!
            bitem?.quantity += 1
            
            if !prdCounter.keys.contains(tran.product!) {
                prdCounter[tran.product!] = 0
            }
            prdCounter[tran.product!] = prdCounter[tran.product!]! + 1
        }
        
        bill.items = mapping.values.sorted(by: { (i1, i2) -> Bool in
            return i1.name > i2.name
        })
        
        self.productCounter = prdCounter
    }
    private func syncAllAmounts(){
        if self.bill == nil { return }
        
        let bill = self.bill!
        
        bill.total = bill.items.reduce(0, { (sum, item) -> Double in
            return sum + item.subtotal
        })
    }
}

class Bill : EVObject {
    // consumer ordering
    static let STATE_NEW = "NEW"
    static let STATE_CANCELED = "CANCELED"
    // provider dealing
    static let STATE_PREPARING = "PREPARING"
    static let STATE_WAIT_FOR_PAYMENT = "WAIT_FOR_PAYMENT"
    static let STATE_PAIED = "PAIED"
    static let STATE_DELETED = "DELETED"
    // provider finishing
    static let STATE_COMPLETED = "COMPLETED"
    static let STATE_CLOSED = "CLOSED"

    var createTime = Date().timeIntervalSince1970
    var requestTime = 0.0  // time when order been requested online
    var deliverTime = 0.0  // time when product been delivered
    var checkoutTime = 0.0 // time when customer checkout & pay
    var completeTime = 0.0 // time when moved to bill history
    var closeTime = 0.0    // time when provider confirm this record
    var updateTime = 0.0   // time whenever any content been updated
    var logs = [String]()
    
    var sequence = 0
    var displayId = ""
    var id = ""
    
    var providerId = ""
    var providerInfo = Providerinfo()
    var consumerId = ""
    var consumerInfo = ConsumerInfo()
    
    var items = [BillItem] ()
    var state = STATE_NEW
    var total = 0.0
    
    var payments = [Payment]()
}

class Providerinfo : EVObject {
    var name = ""
    var phone = ""
    var address = ""
    var businessId = ""
    var ownerId = ""
}

class ConsumerInfo : EVObject {
    static let DELIVER_PICKUP = "PICKUP"
    static let DELIVER_PACKAGE = "SEND"
    
    var name = ""
    var phone = ""
    var address = ""
    var preferDeliverTime = Date().timeIntervalSince1970
    var deliverTime = Date().timeIntervalSince1970
    var deliverType = DELIVER_PICKUP
    var deliverDetail:NSDictionary?
    var memo = ""
}

class Payment : EVObject {
    static let PAY_CASH = "CASH"
    static let PAY_CREDIT_CARD = "CREDIT_CARD"
    
    var method = PAY_CASH
    var amount = 0.0
    var changes = 0.0
}
   
class BillItem : EVObject {
    var name = ""
    var price = 0.0
    var quantity = 0.0
    var subtotal:Double {
        return price * quantity
    }
    var options = [Option]()
}

class Transaction {
    var product: Product!
    var price:Double { return product.price }
    var options = [Option]()
    init(product:Product) {
        self.product = product
    }
}
