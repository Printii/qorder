var admin = require("firebase-admin")
var databaseURL = "https://quickorder-ff7aa.firebaseio.com";

admin.initializeApp({
  credential: admin.credential.cert("quickorder-ff7aa-firebase-adminsdk-7c1bk-e6cb275a4c.json"),
  databaseURL: databaseURL
});

var db = admin.database()
var messaging = admin.messaging()

var rootRef = db.ref('orders')
    .on('child_added', function(data){
        var stateSnapshot = data.child('state');
        if(stateSnapshot.val() == 'NEW') {
            console.log('PROCESSING:' + data.key);
            stateSnapshot.ref.set('WAIT_FOR_PAYMENT');

            var order = data.val();
            var providerInfo = data.child('providerInfo').val();
            var consumerInfo = data.child('consumerInfo').val();
            var total = data.child('total').val();
            db.ref('users/' + providerInfo.ownerId + '/device').on('value', function(deviceSnapshot){
                var deviceToken = deviceSnapshot.val()
                var title = consumerInfo.name + "向您預約餐點 計$" + total + "元";
                var body = "";
                for(var key in order.items) {
                    var item = order.items[key]
                    body += [item.name, ' x', item.quantity, '\n'].join('');
                }

                var payload = {
                    notification: {
                        title: title,
                        body: body
                    }
                };
                console.log("Sending message to: ", deviceToken);
                messaging.sendToDevice(deviceToken, payload)
                    .then(function(response){
                        console.log("Successfully sent message:", response);
                    })
                    .catch(function(error){
                        console.log("Error sending message:", error);
                    });  
            });
        }
    }
)